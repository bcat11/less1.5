FROM debian:bullseye-slim

ENV APP_NAME dind
ENV APP_INSTALL_PATH /opt/${APP_NAME}

WORKDIR ${APP_INSTALL_PATH}

RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl gnupg bash && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    bullseye stable' | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt-get update && apt-get install -y  docker-ce docker-ce-cli containerd.io && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

COPY start.sh .

ENTRYPOINT [ "./start.sh" ]
