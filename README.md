Контейнер dind

Клонируем репо

	git clone https://gitlab.com/bcat11/less1.5.git

Запуск контейнера

	docker run --privileged -it my_dind

Что бы исправить ошибку с драйвером overla2, для запуска использовать требуется использовать unix socket /var/run/docker.sock

Команда для запуска в данном случае буде выглядить так:

	docker run --rm -ti --privileged -v /var/run/docker.sock:/var/run/docker.sock my_dind


